<?php
namespace App\Http\Controllers\Web;

use App\Model\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $Items = Item::get();
        $Products = [];

        foreach ($Items as $item) {
            $Products[$item->SKU] = $item;
        }

        $name = array_column($Products, 'item_name');
        array_multisort($name, SORT_ASC, $Products);

        return $Products;
    }
}
