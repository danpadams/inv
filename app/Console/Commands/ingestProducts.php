<?php

namespace App\Console\Commands;

use App\Model\Item;
use App\Model\InventoryHistory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ingestProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:products {source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ingest Products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sourceFile = $this->argument('source');

        $csv = $this->arrayFromCsv($sourceFile);

//        echo print_r($csv, true);

//        $row = $csv[0];
        foreach ($csv as $row) {
            $Item = Item::where('token', '=', $row['Token'])->first();
            if (empty($Item)) {
                $Item = new Item();
                $Item->token = $row['Token'];
            }
            $Item->sku = $row['SKU'];
            if (empty($row['Current Quantity Glamour Nails by Jamie'])) {
                $row['Qty'] = 0;
            } else {
                $row['Qty'] = $row['Current Quantity Glamour Nails by Jamie'];
            }
            $this->recordQty($row['SKU'], $row['Qty'], $Item->qty);
            $Item->qty = $row['Qty'];
            $Item->item_name = $row['Item Name'];
            $Item->category = $row['Category'];
            if (!is_numeric($row['Price'])) {
                $Item->price = 0;
            } else {
                $Item->price = $row['Price'];
            }

            $Item->save();
        }

        return 0;
    }

    /**
     * @param $filename
     * @return array
     */
    private function arrayFromCsv($filename)
    {
        $rows = array_map('str_getcsv', file($filename));
        $header = array_shift($rows);
        $csv = array();
        foreach ($rows as $row) {
            $csv[] = array_combine($header, $row);
        }

        return $csv;
    }

    private function recordQty($sku, $qty_new, $qty_former)
    {
        $recount = true;
        if (empty($sku)) {
            return;
        }
        if ($qty_former == $qty_new) {
            echo $sku . " - No Change in Qty\n";
            return;
        }

        if ($recount) {
            if ($qty_new != 0 || $qty_former != 0) {
                $InventoryHistory = new InventoryHistory();
                $InventoryHistory->SKU = $sku;
                $InventoryHistory->asof = date('Y-m-d');
                $InventoryHistory->reason_id = 1;
                $InventoryHistory->qty = $qty_new;
                $InventoryHistory->save();
            }
        } else {

        }
    }
}
