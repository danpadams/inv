<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200" ng-controller="defaultLayoutCtrl">
                    <form ng-submit="submitBarcode()">
                        Add Barcode Here<input ng-model="barcode" type="text"><br>
                        <input type="radio" checked ng-model="direction" value="1">Add<br>
                        <input type="radio" ng-model="direction" value="-1">Remove<br>


                    </form>
                    <hr>
                    <table width="100%">
                        <tr>
                            <td with="25%">SKU</td>
                            <td with="25%">@{{SKU}}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>@{{ dataSet[SKU]['item_name'] }}</td>
                        </tr>
                        <tr>
                            <td>QTY</td>
                            <td>@{{ dataSet[SKU]['qty'] }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>@{{ dataSet[SKU]['Dup'] }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>@{{ dataSet[SKU]['status'] }}</td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td>@{{ data[SKU]['category'] }}</td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>@{{ data[SKU]['price'] }}</td>
                        </tr>
                    </table>
                    <hr>

                    <table style="width: 100%;">
                        <tr>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>Status</th>
                            <th>Active Qty</th>
                            <th>Square Qty</th>
                            <th>Category</th>
                            <th>Price</th>
                        </tr>
                        <tr ng-repeat="dat in dataSet" ng-if="showItem(dat)">
                            <td>@{{ dat.item_name }}</td>
                            <td>@{{ dat.SKU }}</td>
                            <td>@{{ dat.status }}</td>
                            <td>@{{ dat.qty }}</td>
                            <td>@{{ dat.square_qty }}</td>
                            <td>@{{ dat.category }}</td>
                            <td>@{{ dat.price }}</td>
                        </tr>
                    </table>
                    <hr>
{{--                                        @{{ dataSet }}--}}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
