<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>


        <script type="text/javascript" src="http://control.reachoutwizard.com/js/jquery-1.11.3.js"></script> <!-- Datepicker -->
        <script type="text/javascript" src="http://control.reachoutwizard.com/js/bootstrap.js"></script> <!-- Datepicker -->
        <script type="text/javascript" src="http://control.reachoutwizard.com/lib/angular.min.1.4.7.js"></script>

        <script type="text/javascript">
            var infoChi = angular.module("infoChi", [
                // "ngRoute",
                // "ngStorage",
                // "ngFlash",
                // 'ngSanitize',
                // 'ui.select',
                // 'angularSpinners',
                // 'angularUtils.directives.dirPagination',
                // 'ui.bootstrap.datetimepicker',
                // 'ui.bootstrap'
            ]);
        </script>
    <script src="{{ asset('js/index.js') }}"></script>
    </head>

    <body class="font-sans antialiased" ng-app="infoChi">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
</html>
