<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('token', 128);
            $table->string('SKU', 32)->default('');
            $table->string('item_name', 128)->default('');
            $table->string('category', 32)->default('');
            $table->string('price', 5,2)->default(0);
            $table->unsignedDecimal('qty', 5, 2)->defualt(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
};
