infoChi.controller("defaultLayoutCtrl", ['$scope', '$http', function ($scope, $http) {
    $scope.submitBarcode = function () {
        console.log("Direction: " + $scope.direction);
        let SKU = $scope.barcode;

        // Add Qty for Scan
        if ($scope.dataSet[SKU] == undefined || $scope.dataSet[SKU]['qty'] == 0) {
            if ($scope.dataSet[SKU] == undefined) {
                $scope.dataSet[SKU] = {};
            }
            $scope.dataSet[SKU]['SKU'] = SKU;
            $scope.dataSet[SKU]['qty'] = 1 * $scope.direction;
            $scope.dataSet[SKU]['Dup'] = '';
            if ($scope.dataSet[SKU]['square_qty'] == undefined) {
                $scope.dataSet[SKU]['square_qty'] = 0;
                if ($scope.data[SKU]) {
                    if ($scope.data[SKU]['price']) {
                        $scope.dataSet[SKU]['price'] = $scope.data[SKU]['price'];
                    }
                    if ($scope.data[SKU]['category']) {
                        $scope.dataSet[SKU]['category'] = $scope.data[SKU]['category'];
                    }
                    if ($scope.data[SKU]['item_name']) {
                        $scope.dataSet[SKU]['item_name'] = $scope.data[SKU]['item_name'];
                    }
                }
            }
            // Call for Check Status
            checkStatus(SKU);
        } else {
            $scope.dataSet[SKU]['qty'] = $scope.dataSet[SKU]['qty'] + (1 * $scope.direction);
            if ($scope.dataSet[SKU]['qty'] > 1) {
                $scope.dataSet[SKU]['Dup'] = 'Duplicate';
            } else {
                $scope.dataSet[SKU]['Dup'] = '';
            }
        }

        $scope.SKU = SKU;
        setup();
        console.log($scope.dataSet);
    }

    setup = function () {
        $scope.barcode = '';
        $scope.direction = 1;
    }
    checkStatus = function(SKU) {
        $scope.dataSet[SKU]['status'] = 'Status';
        $http.get('https://www.colorstreet.com/glamournailsbyjamie/product/' + SKU).then(function (response) {

            body = new DOMParser().parseFromString(response.data, 'text/html');
            value = body.querySelector('script').innerText; // find <code> tag and get text
            pos = value.search("'page_name'");
            checkVal = value.substring(pos+12);
            var matches = checkVal.match(/\'(.*?)\'/);  //returns array
            console.log(matches[0]);
            let name = matches[0].slice(0, -14);
            $scope.dataSet[SKU]['Name'] = name.slice(1);

            if ($scope.dataSet[SKU]['Name'] == 'Shopping') {
                $scope.dataSet[SKU]['status'] = 'Retired';
            } else {
                $scope.dataSet[SKU]['status'] = 'Current';
                $scope.dataSet[SKU]['item_name'] = $scope.dataSet[SKU]['Name'];
                // console.log('Name');
            }
            console.log(matches[0].slice(0, -14));
            console.log(checkVal);
            console.log(value);
        });
        console.log('checkStatus()');
        console.log(SKU);
    }
    checkDuplicate = function (SKU) {
        $scope.dataSet[SKU]['Dup'] = ''
    }
    getData = function () {
        // infoChiSpinner.show();
        $http.get('/web/products').then(function (response) {
            $scope.data = response.data;
            console.log($scope.data.size);

            for (const [key, value] of Object.entries(response.data)) {
                // if (value['qty'] != '0') {
                    SKU = key;
                    $scope.dataSet[SKU] = [];
                    $scope.dataSet[SKU]['SKU'] = SKU;
                    $scope.dataSet[SKU]['item_name'] = value['item_name'];
                    $scope.dataSet[SKU]['qty'] = 0;
                    $scope.dataSet[SKU]['square_qty'] = value['qty'];
                    $scope.dataSet[SKU]['category'] = value['category'];
                    $scope.dataSet[SKU]['price'] = value['price'];
                // }

            }
            // for (i = 0; i < response.data.size(); i++) {
              // }
            // infoChiSpinner.hide();
        });
    };
    $scope.getData = function () {
        getData();
    }
    $scope.showItem = function (item) {
        if ((item.qty == 0) && (item.square_qty == 0)) {
            return false;
        }
        return true;
    }

    getData();
    $scope.direction = 1;
    $scope.dataSet = {};
}]);
